'use strict';
var xo = xo || {};

(() => {
  xo.winChecker = () => {
    let getAllCells = (rows) => {
      let cells = [];
      rows.forEach((row) => { row.forEach((cell) => { cells.push(cell); }); });
      return cells;
    };

    let getRow = (cell, cells) => {
      return cells.filter((c) => { return c.y === cell.y; });
    };

    let getCol = (cell, cells) => {
      return cells.filter((c) => { return c.x === cell.x; });
    };

    let cellContentMatches = (cell, cells) => {
      return cells.every((c) => { return c.content === cell.content; });
    };

    let allCellsPlayed = (cells, defaultCellContent) => {
      return cells.every((c) => { return c.content !== defaultCellContent; });
    };

    let checkForWinState = (game, cell) => {
      let winner = undefined;

      let cells = getAllCells(game.rows);
      let thisRowWins = cellContentMatches(cell, getRow(cell, cells));
      let thisColWins = cellContentMatches(cell, getCol(cell, cells));
      let topLeftToBottomRightWins = cellContentMatches(cell, game.topLeftToBottomRight);
      let topRightToBottomLeftWins = cellContentMatches(cell, game.topRightToBottomLeft);

      if (thisRowWins) {
        winner = cell.content + ' WINS! (row)';
      } else if (thisColWins) {
        winner = cell.content + ' WINS! (column)';
      } else if (topLeftToBottomRightWins) {
        winner = cell.content + ' WINS! (top left to bottom right)';
      } else if (topRightToBottomLeftWins) {
        winner = cell.content + ' WINS! (top right to bottom left)';
      } else if (allCellsPlayed(cells, game.defaultCellContent)) {
        winner = 'DRAW';
      }

      game.winner = winner;
      if (winner) {
        game.state = 'finished';
      }

      return winner;
    };

    return {
      checkForWinState: checkForWinState
    };
  };
})();
