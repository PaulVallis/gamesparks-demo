'use strict';
var xo = xo || {};

(() => {
  xo.gameBoard = (state) => {
    let rows = []
    let topLeftToBottomRight = [];
    let topRightToBottomLeft = [];

    let id = 0;
    let y = 0;
    let tltbrIndex = 0;
    let trtblIndex = state.size - 1;
    for (let rowIndex = 0; rowIndex < state.size; rowIndex++) {
      let x = 0;
      let row = [];
      for (let colIndex = 0; colIndex < state.size; colIndex++) {
        row.push({id: id, x: x, y: y, content: state.defaultCellContent });
        x++;
        id++;
      }

      topLeftToBottomRight.push(row[tltbrIndex]);
      topRightToBottomLeft.push(row[trtblIndex]);
      rows.push(row);

      tltbrIndex++;
      trtblIndex--;
      y++;
    }

    return {
      rows: rows,
      topLeftToBottomRight: topLeftToBottomRight,
      topRightToBottomLeft: topRightToBottomLeft
    };
  };
})();
