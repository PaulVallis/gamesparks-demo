'use strict';
var xo = xo || {};

(() => {
  xo.getNewGameBoard = (size, defaultCellContent) => {
    let state = {
      size: size,
      defaultCellContent: defaultCellContent,
      state: 'stopped',
      currentPlayer: undefined,
      players: [],
      winner: undefined
    };

    var board = xo.gameBoard(state);

    return Object.assign(
      {},
      state,
      board
    );
  };

  xo.actions = () => {
    return Object.assign(
      {},
      xo.playerActions(),
      xo.gameActions(),
      xo.winChecker()
    );
  };
})();
