'use strict';
var xo = xo || {};

(() => {
  xo.gameActions = () => {
    let start = (game) => {
      if (game.players.length !== 2) {
        throw 'Game must have 2 players to start';
      }

      game.state = 'started';
      game.currentPlayer = game.players[0];
    };

    return {
      start: start
    };
  };
})();
