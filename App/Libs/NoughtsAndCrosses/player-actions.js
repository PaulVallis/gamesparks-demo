'use strict';
var xo = xo || {};

(() => {
  xo.playerActions = () => {
    let playTurn = (game, cell) => {
      if (game.state !== 'started') {
        return false;
      }

      if (cell.content !== game.defaultCellContent) {
        return false
      }

      cell.content = game.currentPlayer;
      return true;
    };

    let switchPlayer = (game) => {
      game.currentPlayer = game.players.filter((p) => { return p !== game.currentPlayer; })[0];
    };

    let addPlayer = (game, player) => {
      if (game.players.lenth <= 2) {
        throw 'A game cannot have more than 2 players';
      }
      game.players.push(player);
    };

    return {
      playTurn: playTurn,
      switchPlayer: switchPlayer,
      addPlayer: addPlayer
    };
  };
})();
