'use strict';
var racetrack = racetrack || {};

racetrack.players = (() => {
  let racetrackPlayerManagement = (state, options) => {
    let drawPlayer = (player) => {
      let circle = state.paper.circle(0, 0, 15);
      circle.attr('fill', player.Fill);
      circle.attr('stroke', '#000');
      circle.attr('stroke-width', 2);
      setUpTrackAnimation(circle, player.Start);
      return circle;
    };

    let setUpTrackAnimation = (svgElement, startPosition) => {
      svgElement.attr({ along: startPosition });
      svgElement.onAnimation(() => {
        let t = svgElement.attr('transform');
        svgElement.attr({ path: t[0][1] + ',' + t[0][2] + 'z' });
      });
    };

    let trackAnimationFactory = (time, endPosition) => {
      return Raphael.animation({ along: endPosition || 1 }, time).repeat(1);
    };

    let runAnimation = (svgElement, time, endPosition) => {
      svgElement.animate(trackAnimationFactory(time, endPosition, 1), () => {
        svgElement.attr({ along: 0 });
      });
    };

    let runAllAnimations = () => {
      state.players.forEach((player) => {
        runAnimation(player.Svg, player.Time, player.End);
      });
    };

    let addPlayer = (fill, start, end, time) => {
      var player = { Fill: fill, Start: start, End: end, Time: time };
      var svg = drawPlayer(player);
      player.Svg = svg;

      state.players.push(player);
    };

    let getPlayers = () => {
      return state.players;
    };

    return {
      addPlayer,
      runAllAnimations,
      getPlayers
    };
  };

  return racetrackPlayerManagement;
})();
