'use strict';
var racetrack = racetrack || {};

racetrack.core = (() => {
  let racetrackCore = (state, options) => {
    let drawTrack = () => {
      state.track = state.paper.path(state.trackPath);
      state.track.attr('stroke', options.trackFill);
      state.track.attr('stroke-width', 30);

      let trackMarkings = state.paper.path(state.trackPath);
      trackMarkings.attr('stroke', options.trackLineFill);
      trackMarkings.attr('stroke-width', 3);
      trackMarkings.attr('stroke-dasharray', '- ');

      state.paper.setSize(state.paper.width, state.paper.height);
    };

    let enableResponsiveChart = () => {
      state.paper.setViewBox(0, 0, options.width, options.height, false);
      state.paper.width = '100%';
      state.paper.height = options.initialChartHeight;
    };

    let addCustomAttributes = () => {
      state.paper.customAttributes.along = (v) => {
        let point = state.track.getPointAtLength(v * state.track.getTotalLength());
        return {
          transform: 't' + [point.x, point.y] + 'r' + point.alpha
        };
      };
    };

    let setupElement = () => {
      let chartElement = document.getElementById(options.elementId);
      chartElement.innerHTML = "";

      state.paper = new Raphael(chartElement);
    };

    let init = () => {
      setupElement();
      drawTrack();
      enableResponsiveChart();
      addCustomAttributes();
    };

    return {
      init: init
    };
  };

  return racetrackCore;
})();
