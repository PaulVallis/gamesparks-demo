﻿'use strict';

var experiments = experiments || {
    app: 'experiments',
    authCookieKey: 'g2g3-auth-cookie'
};

// GameSparks functions seem to expect the gamesparks instance to be globally available
var gamesparks = new GameSparks();

// ShortCodes are used all over the place in GameSparks
// They are essentially magic strings that identify parts of the GS service
GameSparks.prototype.ShortCodes = {
  InitBroadcast: 'INIT_BROADCAST', // I made this one up so I can send a broadcast message when GS init is done
  ScoreEvent: 'SCORE_EVT',
  ScoreEventAttribute: 'SCORE_ATTR',
  HighScoreLeaderboard: 'HIGH_SCORE_LB',
  LeaderboardUpdateRequired: 'LEADERBOARD_PUSH',
  Achievement10KPoints: 'ACH_10K_POINTS',
  Achievement100KPoints: 'ACH_100K_POINTS',
  Achievement500KPoints: 'ACH_500K_POINTS',
  AchievementFirstPlace: 'ACH_1ST_PLACE',
  RequestAchievements: 'REQ_ACHIEVEMENTS',
  AllAchievements: 'ALL_ACHIEVEMENTS',
  GetProfile: 'GET_PROFILE',
  UpdateProfileField: 'UPDATE_PROFILE_FIELD',
  UpdateProfileFieldNameAttr: 'FIELD_NAME',
  UpdateProfileFieldValueAttr: 'FIELD_VALUE'
};

// These identify us to GS, they shouldn't be here really
GameSparks.prototype.API = {
  Key: '294106CjselF',
  Secret: 'xyYdEN3B25c8hLk5W5GcUNy2jRTK2hSp'
};

GameSparks.prototype.ShortCodeAliases = {
  '.InitBroadcast': 'shortCode', // I made this class type up too
  '.AchievementEarnedMessage': 'achievementShortCode',
  '.ScriptMessage': 'extCode'
};
