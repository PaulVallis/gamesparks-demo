﻿(function () {
    var app = angular.module(experiments.app);

    app.directive('leaderboard', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/Leaderboard/leaderboard.html',
            scope: {},
            controller: ['$scope', 'gameSparksServiceFactory', function ($scope, gameSparksServiceFactory) {
              var gameSparksService = gameSparksServiceFactory();

              var handleError = function (response) {
                $scope.errorMessage = 'Something went wrong while getting the Leaderboard';
              };

              var displayLeaderboard = function (response) {
                $scope.leaderboard = response.data;
              };

              var getLeaderboard = function () {
                $scope.errorMessage = undefined;

                var entryCount = 50;
                gameSparksService.getLeaderboard(entryCount, function (response) {
                  if (response.error) {
                    handleError(response);
                  } else {
                    displayLeaderboard(response);
                  }

                  //todo: $scope.$apply(); is required here are we're in a callback, wrapping gamesparks calls as promises might help
                  $scope.$apply();
                });
              };

              gameSparksService.subscribeToMessage(gamesparks.ShortCodes.LeaderboardUpdateRequired, (message) => {
                getLeaderboard();
              });

              $scope.getLeaderboard = function () {
                getLeaderboard();
              };
            }]
        }
    }]);
})();
