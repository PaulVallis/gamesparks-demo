﻿(function () {
  'use strict';

    var app = angular.module(experiments.app);

    app.directive('authenticate', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/Authenticate/authenticate.html',
            scope: {},
            controller: ['$scope', '$location', '$rootScope', 'authServiceFactory', 'cookieServiceFactory', 'gameSparksServiceFactory', function ($scope, $location, $rootScope, authServiceFactory, cookieServiceFactory, gameSparksServiceFactory) {
              var authService = authServiceFactory();
              var cookieService = cookieServiceFactory();
              var gameSparksService = gameSparksServiceFactory();
              let subscriberId = undefined;

              var logInAndRedirectToLeaderboard = (displayName) => {
                $rootScope.Authenticated = true;
                $rootScope.Username = displayName;

                var path = $rootScope.postLoginRedirectLocation || '/';
                $location.path(path);

                $scope.$apply();
              };

              var handleError = (response) => {
                $scope.errorMessage = 'Login failed. Please try again';
                $scope.$apply();
              };

              var handleSuccess = (response) => {
                logInAndRedirectToLeaderboard(response.displayName);
              };

              var auth = (username, password) => {
                $scope.errorMessage = undefined;
                $scope.displayName = undefined;

                authService.auth(username, password, handleSuccess, handleError);
              };

              $scope.auth = () => {
                auth($scope.username, $scope.password);
              };

              var init = () => {
                var cookie = cookieService.getCookie(experiments.authCookieKey);
                if (!cookie || !cookie.username || !cookie.password) {
                  return;
                };

                subscriberId = gameSparksService.subscribeToMessage(gamesparks.ShortCodes.InitBroadcast, () => {
                  auth(cookie.username, cookie.password);
                });
              }();

              $scope.$on('$destroy', () => {
                if (subscriberId) {
                  gameSparksService.unsubscribe(subscriberId);
                }
              });
            }]
        }
    }]);
})();
