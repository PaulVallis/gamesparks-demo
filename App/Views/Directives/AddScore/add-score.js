﻿(function () {
    var app = angular.module(experiments.app);

    app.directive('addScore', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/AddScore/add-score.html',
            scope: {},
            controller: ['$scope', 'gameSparksServiceFactory', function ($scope, gameSparksServiceFactory) {
              var gameSparksService = gameSparksServiceFactory();

              var handleError = function (response) {
                $scope.errorMessage = 'Could not submit score';
              };

              var handleSuccess = function (response) {
                $scope.successMessage = 'Score saved!';
              };

              $scope.addScore = function () {
                $scope.errorMessage = undefined;
                $scope.successMessage = undefined;

                gameSparksService.logNewScore($scope.score, function (response) {
                  if (response.error) {
                    handleError(response);
                  } else {
                    handleSuccess(response);
                  }

                  //todo: $scope.$apply(); is required here are we're in a callback, wrapping gamesparks calls as promises might help
                  $scope.$apply();
                });
              };
            }]
        }
    }]);
})();
