﻿(function () {
    var app = angular.module(experiments.app);

    app.directive('signUp', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/SignUp/sign-up.html',
            scope: {},
            controller: ['$scope', 'gameSparksServiceFactory', function ($scope, gameSparksServiceFactory) {
              var gameSparksService = gameSparksServiceFactory();

              var handleError = function (response) {
                $scope.errorMessage = 'Registration failed. Please try again';
              };

              var handleSuccess = function (response) {
                $scope.newUserDisplayname = response.displayName;
              };

              $scope.register = function () {
                $scope.errorMessage = undefined;
                $scope.newUserDisplayname = undefined;

                gameSparksService.registrationRequest($scope.displayName, $scope.password, $scope.username, function (response) {
                  if (response.error) {
                    handleError(response);
                  } else {
                    handleSuccess(response);
                  }

                  //todo: $scope.$apply(); is required here are we're in a callback, wrapping gamesparks calls as promises might help
                  $scope.$apply();
                });
              };
            }]
        }
    }]);
})();
