﻿(function () {
  'use strict';

  var app = angular.module(experiments.app);

  app.directive('profileFormField', [function () {
    return {
      restrict: 'E',
      templateUrl: '/App/Views/Directives/ProfileFormField/profile-form-field.html',
      scope: {
        field: '=',
        bonus: '=',
        total: '='
      },
      controller: ['$scope', 'gameSparksServiceFactory', function ($scope, gameSparksServiceFactory) {
        let gameSparksService = gameSparksServiceFactory();
        let subscriberId = undefined;

        let setValidationMark = () => {
          $scope.field.Valid ?
            $scope.ValidationMark = '✔' :
            $scope.ValidationMark = '✘';
        };

        let setValues = (messageData, bonus, total) => {
          $scope.field.Valid = messageData.Valid;
          $scope.field.Value = messageData.Value;
          $scope.field.CoinsAwarded = messageData.CoinsAwarded;

          $scope.bonus = bonus;
          $scope.total = total;

          setValidationMark();
        };

        let save = () => {
          if (!subscriberId) {
            subscriberId = gameSparksService.subscribeToMessage(gamesparks.ShortCodes.UpdateProfileField, (message) => {
              if (!message.data.Success) {
                return;
              }

              if (message.data.Field.Name !== $scope.field.Name) {
                return;
              }

              setValues(message.data.Field, message.data.Bonus, message.data.Total);

              $scope.$apply();
            });
          }

          var request = {};
          request[gamesparks.ShortCodes.UpdateProfileFieldNameAttr] = $scope.field.Name;
          request[gamesparks.ShortCodes.UpdateProfileFieldValueAttr] = $scope.field.Value;

          gameSparksService.makeEventRequest(gamesparks.ShortCodes.UpdateProfileField, request);
        };

        $scope.$watch('field.Value', (value) => {
          if (value !== null && value !== undefined & value !== '') {
            save();
          }
        });

        $scope.$on('$destroy', () => {
          if (subscriberId) {
            gameSparksService.unsubscribe(subscriberId);
          }
        });

        let init = () => {
          setValidationMark();
        }();
      }]
    };
  }]);
})();
