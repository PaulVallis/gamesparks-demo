﻿(() => {
    'use strict';
    var app = angular.module(experiments.app);

    app.directive('racetrackChart', [() => {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/RacetrackChart/racetrack-chart.html',
            scope: {},
            controller: ['$scope', '$interval', ($scope, $interval) => {
              let track = undefined;
              let loop = undefined;

              $scope.go = () => {
                if (!track) {
                  return;
                }
                $scope.gone = true;
                track.runAllAnimations();
              };

              $scope.reset = () => {
                init();
              };

              let rand = (min, max) => {
                return Math.round((Math.random() * (max - min) + min) * 100) / 100;
              };

              let addPlayers = () => {
                ['#ff0000','#00ff00','#0000ff','#ffff00','#ff00ff','#00ffff','#aa00aa','#00aa00','#abcdef','#123456'].forEach((colour) => {
                  track.addPlayer(colour, rand(0, 0.5), rand(0.55, 1), rand(5000, 10000));
                });
              };

              $scope.ordinalSuffixOf = (i) => {
                let j = i % 10,
                k = i % 100;
                if (j == 1 && k != 11) {
                  return i + "st";
                }
                if (j == 2 && k != 12) {
                  return i + "nd";
                }
                if (j == 3 && k != 13) {
                  return i + "rd";
                }
                return i + "th"
              }

              let drawPlayers = () => {
                $scope.players = track.getPlayers().map((player) => {
                  return {
                    Fill: player.Fill,
                    Dist: player.Svg.attrs.along,
                    Percent : player.Svg.attrs.along * 100
                  };
                });
              };

              let startEventLoop = () => {
                let fps = 5;

                loop = $interval(() => {
                  drawPlayers();
                }, 1000 / fps);
              };

              let stopEventLoop = () => {
                if (loop) {
                  $interval.cancel(loop)
                }
              };

              let init = () => {
                $scope.gone = false;

                let options = {
                  elementId: 'racetrack-chart',
                  trackFill: '#488',
                  trackLineFill: '#fff',
                  width: 1800,
                  height: 900
                };

                track = racetrack.factory(options);
                track.init();
                addPlayers();

                startEventLoop();
              };

              init();

              $scope.$on('$destroy', () => {
                stopEventLoop();
              });
            }]
        }
    }]);
})();
