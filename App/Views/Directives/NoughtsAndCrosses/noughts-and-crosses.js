﻿(function () {
    'use strict';
    var app = angular.module(experiments.app);

    app.directive('noughtsAndCrosses', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/NoughtsAndCrosses/noughts-and-crosses.html',
            scope: {},
            controller: ['$scope', function ($scope) {
              $scope.restart = () => {
                $scope.game = xo.getNewGameBoard(3, '');
                xo.actions().addPlayer($scope.game, 'X');
                xo.actions().addPlayer($scope.game, 'O');
              };

              $scope.start = () => {
                xo.actions().start($scope.game);
              };

              $scope.playTurn = (cell) => {
                let played = xo.actions().playTurn($scope.game, cell);
                if (!played) {
                  return;
                }

                var winner = xo.actions().checkForWinState($scope.game, cell);
                if (winner) {
                  return;
                }

                xo.actions().switchPlayer($scope.game);
              };

              $scope.restart();
            }]
        };
    }]);
})();
