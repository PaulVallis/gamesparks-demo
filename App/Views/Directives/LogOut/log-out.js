﻿(function () {
    'use strict';
    var app = angular.module(experiments.app);

    app.directive('logOut', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/LogOut/log-out.html',
            scope: {},
            controller: ['$scope', 'cookieServiceFactory', '$location', '$rootScope', 'gameSparksServiceFactory', function ($scope, cookieServiceFactory, $location, $rootScope, gameSparksServiceFactory) {
              let cookieService = cookieServiceFactory();
              let gameSparksService = gameSparksServiceFactory();

              $scope.logOut = () => {
                cookieService.deleteCookie(experiments.authCookieKey);
                gameSparksService.logOut();
                $rootScope.Username = undefined;
                $location.path('/');
              };
            }]
        }
    }]);
})();
