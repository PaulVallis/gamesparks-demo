﻿(() => {
  'use strict';

  var app = angular.module(experiments.app);

  app.directive('profileForm', [() => {
    return {
      restrict: 'E',
      templateUrl: '/App/Views/Directives/ProfileForm/profile-form.html',
      scope: {},
      controller: ['$scope', '$rootScope', 'gameSparksServiceFactory', ($scope, $rootScope, gameSparksServiceFactory) => {
        let gameSparksService = gameSparksServiceFactory();
        let broadcastSubscriberId = undefined;
        let messageSubscriberId = undefined;

        let calcPercentComplete = () => {
          if (!$scope.profile) {
            return;
          }

          let fieldCount = $scope.profile.fields.length;
          let completeFieldCount = $scope.profile.fields.filter((field) => { return field.Valid }).length;

          $scope.percentComplete = completeFieldCount / fieldCount * 100;
        };

        let setProfile = (data) => {
          $scope.profile = data;
          calcPercentComplete();
          $scope.$apply();
        };

        let getProfileData = () => {
          broadcastSubscriberId = gameSparksService.subscribeToMessage(gamesparks.ShortCodes.GetProfile, (message) => {
            if (message.data) {
              setProfile(message.data)
            } else {
              console.log('Something broke there fella...');
            }
          });

          gameSparksService.makeEventRequest(gamesparks.ShortCodes.GetProfile);
        };

        let init = () => {
          if ($rootScope.GameSparksReady) {
            getProfileData();
          } else {
            gameSparksService.subscribeToMessage(gamesparks.ShortCodes.InitBroadcast, () => {
              getProfileData();
            });
          }
        }();

        $scope.$watch('profile.totalCoins', (newValue) => {
          calcPercentComplete();
        });

        $scope.$on('$destroy', () => {
          if (broadcastSubscriberId) {
            gameSparksService.unsubscribe(broadcastSubscriberId);
          }
          if (messageSubscriberId) {
            gameSparksService.unsubscribe(messageSubscriberId);
          }
        });
      }]
    };
  }]);
})();
