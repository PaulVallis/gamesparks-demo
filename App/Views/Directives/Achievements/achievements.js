﻿(function () {
    var app = angular.module(experiments.app);

    app.directive('achievements', [function () {
        return {
            restrict: 'E',
            templateUrl: '/App/Views/Directives/Achievements/achievements.html',
            scope: {},
            controller: ['$scope', 'gameSparksServiceFactory', '$rootScope', function ($scope, gameSparksServiceFactory, $rootScope) {
              const gameSparksService = gameSparksServiceFactory();

              var achievementUnlocked = (message) => {
                if (!$scope.playerAchievements) {
                  $scope.playerAchievements = [];
                }

                $scope.playerAchievements.push(message.achievementShortCode);
              };

              gameSparksService.subscribeToMessage(gamesparks.ShortCodes.AllAchievements, (message) => {
                $scope.achievements = message.data.Achievements;

                $scope.achievements.forEach((ach) => {
                  setTimeout(gameSparksService.subscribeToMessage(ach.ShortCode, achievementUnlocked), 0);
                });

                $scope.$apply();
              });

              gameSparksService.subscribeToMessage(gamesparks.ShortCodes.RequestAchievements, (message) => {
                $scope.playerAchievements = message.data.Achievements;
                $scope.$apply();
              });

              $scope.markAchieved = (shortCode) => {
                return $scope.playerAchievements.find((ach) => { return ach === shortCode; }) ? 'achieved' : '';
              };

              var init = () => {
                if ($rootScope.GameSparksReady) {
                  gameSparksService.getAchievements();
                } else {
                  gameSparksService.subscribeToMessage(gamesparks.ShortCodes.InitBroadcast, () => {
                    gameSparksService.getAchievements()
                  });
                }
              }();
            }]
        };
    }]);
})();
