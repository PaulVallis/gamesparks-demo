﻿(function () {
    'use strict';
    var app = angular.module(experiments.app);
    app.controller('LogInScreenController', ['$scope', '$rootScope', function ($scope, $rootScope) {
      $rootScope.$watch('Username', (newValue) => {
        $scope.username = $rootScope.Username;
      });
    }]);
})();
