﻿(function () {
    'use strict';

    var app = angular.module(experiments.app);
    app.controller('ProfileScreenController', ['$scope', '$rootScope', '$location', function ($scope, $rootScope, $location) {
      var returnToLogInScreenIfNotAuthenticated = function () {
        if (!$rootScope.Authenticated) {
          $rootScope.postLoginRedirectLocation = '/Profile';
          $location.path('/');
        }
      };

      returnToLogInScreenIfNotAuthenticated();
    }]);
})();
