﻿(function () {
    'use strict';

    var app = angular.module(experiments.app);

    app.controller('LeaderboardScreenController', ['$rootScope', '$location', '$scope', function ($rootScope, $location, $scope) {
      var returnToLogInScreenIfNotAuthenticated = function () {
        if (!$rootScope.Authenticated) {
          $rootScope.postLoginRedirectLocation = '/Leaderboard';
          $location.path('/');
        }
      };

      returnToLogInScreenIfNotAuthenticated();

      $scope.username = $rootScope.Username;
    }]);
})();
