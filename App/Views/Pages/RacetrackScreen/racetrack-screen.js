﻿(() => {
    'use strict';

    var app = angular.module(experiments.app);
    app.controller('RacetrackScreenController', ['$scope', '$rootScope', '$location', ($scope, $rootScope, $location) => {
      var returnToLogInScreenIfNotAuthenticated = () => {
        if (!$rootScope.Authenticated) {
          $rootScope.postLoginRedirectLocation = '/Racetrack';
          $location.path('/');
        }
      };

      returnToLogInScreenIfNotAuthenticated();
    }]);
})();
