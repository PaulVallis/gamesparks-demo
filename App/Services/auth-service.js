(function () {
    var app = angular.module(experiments.app);

    app.factory('authServiceFactory', ['gameSparksServiceFactory', 'cookieServiceFactory', function (gameSparksServiceFactory, cookieServiceFactory) {
        var authService = () => {
          var gameSparksService = gameSparksServiceFactory();
          var cookieService = cookieServiceFactory();

          // Yes I'm storing the password in plain text in a cookie.
          // Yes this is bad practice and makes me an awful human.
          // No I don't care, this is experimental code!
          var setAuthCookie = (username, password) => {
            var cookieData = {
              username: username,
              password: password
            };

            cookieService.setCookie(experiments.authCookieKey, cookieData);
          };

          var auth = (username, password, successCallback, errorCallback) => {
            gameSparksService.authenticationRequest(password, username, (response) => {
              if (response.error) {
                errorCallback(response);
              } else {
                setAuthCookie(username, password);
                successCallback(response);
              }
            });
          };

          var logOut = () => {
            cookieService.deleteCookie(experiments.authCookieKey);
          };

          return {
            auth: auth,
            logOut: logOut
          };
        };

        return authService;
    }]);
})();
