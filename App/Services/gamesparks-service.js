(function () {
  'use strict';

    var app = angular.module(experiments.app);
    var gameSparksMessageReceived = 'gameSparksMessageReceived';

    app.factory('gameSparksServiceFactory', ['$rootScope', function ($rootScope) {

        //--------------- Wrapping for use with Angular -----------------------
        var gameSparksService = function () {
          var messageSubscribers = [];

          var registrationRequest = function (displayName, password, userName, callback) {
            gamesparks.registrationRequest(displayName, password, userName, callback);
          };

          var authenticationRequest = function (password, username, callback) {
            gamesparks.authenticationRequest(password, username, callback);
          };

          var logNewScore = function (value, callback) {
            var request = {};
            request['eventKey'] = gamesparks.ShortCodes.ScoreEvent; // see global.js for definition
            request[gamesparks.ShortCodes.ScoreEventAttribute] = value;

            gamesparks.sendWithData("LogEventRequest", request, callback);
          };

          var getLeaderboard = function (entryCount, callback) {
            var leaderboardShortCode = gamesparks.ShortCodes.HighScoreLeaderboard;

            //todo: work out if any of these can be left out from the request
            var challengeInstanceId = undefined;
            var friendIds = undefined;
            var offset = undefined;
            var social = undefined;

            gamesparks.leaderboardDataRequest(challengeInstanceId, entryCount, friendIds, leaderboardShortCode, offset, social, callback)
          };

          let unsubscribe = (id) => {
            messageSubscribers = messageSubscribers.filter((sub) => {
              return sub.id !== id;
            });
          };

          var subscribeToMessage = function (shortCode, callback) {
            let id = messageSubscribers.length + 1;

            var sub = {
              id: id,
              shortCode: shortCode,
              callback: callback
            };

            messageSubscribers.push(sub);
            return id;
          };

          var runSubscribers = function (message) {
            // GameSparsks in it's wisdom doesn't use consistent naming for ShortCodes
            // Because of this we need to look up the property name being used to
            // reference the ShortCode before we pull it from the message.
            var messageClass = message['@class'];
            var shortCode = message[gamesparks.ShortCodeAliases[messageClass]];

            var subs = messageSubscribers.filter((sub) => {
              return sub.shortCode === shortCode;
            });

            subs.forEach(function (sub) {
              setTimeout(sub.callback(message), 0);
            });
          };

          var getAchievements = (callback) => {
            var request = {};
            request['eventKey'] = gamesparks.ShortCodes.RequestAchievements;

            gamesparks.sendWithData("LogEventRequest", request, callback);
          };

          let makeEventRequest = (eventShortCode, request) => {
            request = request || {};
            request['eventKey'] = eventShortCode;

            gamesparks.sendWithData("LogEventRequest", request);
          };

          let logOut = () => {
            gamesparks.endSessionRequest();
          };

          $rootScope.$on(gameSparksMessageReceived, function (event, data) {
            runSubscribers(data);
          });

          return {
            registrationRequest: registrationRequest,
            authenticationRequest: authenticationRequest,
            logNewScore: logNewScore,
            getLeaderboard: getLeaderboard,
            subscribeToMessage: subscribeToMessage,
            getAchievements: getAchievements,
            makeEventRequest: makeEventRequest,
            unsubscribe: unsubscribe,
            logOut: logOut
          };
        }
        //--------------------------- End Wrapping ----------------------------

        //--------------------- GameSparsk initialisation ---------------------

        var onInit = () => {
          var initBroadcast = {
            '@class': '.InitBroadcast',
            shortCode: gamesparks.ShortCodes.InitBroadcast
          };

          $rootScope.$broadcast(gameSparksMessageReceived, initBroadcast);
          $rootScope.GameSparksReady = true;
        };

        // init using the preview service, this has limitations on usage but is meant for dev/test time
        var initGameSparksService = function () {
          gamesparks.initPreview({
            key:gamesparks.API.Key,
            onNonce: (nonce) => { return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(nonce, gamesparks.API.Secret)) },
            onInit: onInit,
            onMessage: (message) => { $rootScope.$broadcast(gameSparksMessageReceived, message); }
          });
        };

        //---------------------------- End Init -------------------------------

        initGameSparksService();
        return gameSparksService;
    }]);
})();
