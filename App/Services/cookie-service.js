(function () {
    var app = angular.module(experiments.app);

    app.factory('cookieServiceFactory', ['$cookies', function ($cookies) {
        var cookieService = function () {
            var setCookie = function (key, cookieData) {
                $cookies.putObject(key, cookieData);
            };

            var getCookie = function (key) {
                return $cookies.getObject(key);
            };

            var deleteCookie = function (key) {
                $cookies.remove(key);
            };

            return {
                setCookie: setCookie,
                getCookie: getCookie,
                deleteCookie: deleteCookie
            };
        }

        return cookieService;
    }]);
})();
