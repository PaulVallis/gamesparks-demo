﻿experiments.App = (function () {
    var app = angular.module(experiments.app, ['ngRoute', 'ngCookies']);

    app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/Leaderboard', {
            templateUrl: '/App/Views/Pages/LeaderboardScreen/leaderboard-screen.html',
            controller: 'LeaderboardScreenController'
        }).when('/', {
            templateUrl: '/App/Views/Pages/LogInScreen/log-in-screen.html',
            controller: 'LogInScreenController'
        }).when('/Presentation', {
            templateUrl: '/App/Views/Pages/PresentationScreen/presentation-screen.html',
            controller: 'PresentationScreenController'
        }).when('/NoughtsAndCrosses', {
            templateUrl: '/App/Views/Pages/NoughtsAndCrossesScreen/noughts-and-crosses-screen.html',
            controller: 'NoughtsAndCrossesScreenController'
        }).when('/Profile', {
            templateUrl: '/App/Views/Pages/ProfileScreen/profile-screen.html',
            controller: 'ProfileScreenController'
        }).when('/Racetrack', {
            templateUrl: '/App/Views/Pages/RacetrackScreen/racetrack-screen.html',
            controller: 'RacetrackScreenController'
        });
    }]);
});

(function () {
  experiments.App();
})();
